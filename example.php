<?php

require_once 'functions.php';

$messages = [
    "Пароль: 9477
Спишется 123,62р.
Перевод на счет 410011817317829",
    "Пароль: 9477
Спишется 122.65р.
Перевод на счет 410011817317829",
    "Пароль: 1827
Спишется 1р.
Перевод на счет 410011817317829",
    "Спишется 10.5р. Пароль: 1867 
Перевод на счет 410011817317829",
    "Спишется 10.5р. Пароль: 186788
Перевод на счет 410011817317829",
];

foreach ($messages as $message){
    try{
        list($password, $summ, $ymId) = parseMessage($message);
        echo sprintf("Получены данные:\n
        Пароль проведения операции: %s\n
        Сумма: %s\n
        Кошелек:%s\n", $password,$summ, $ymId).PHP_EOL.PHP_EOL;
    } catch (\InvalidArgumentException $e){
        echo $e->getMessage().PHP_EOL;
    }

}