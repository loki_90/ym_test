#!/env/php
<?php

require_once 'functions.php';

$message = $argv[1] ?? null;
if (is_null($message)){
    echo "usage: php ym_parse.php message".PHP_EOL;
    exit(1);
}

try{
    list($password, $summ, $ymId) = parseMessage($message);
    echo sprintf("Получены данные:\n
        Пароль проведения операции: %s\n
        Сумма: %s\n
        Кошелек:%s\n", $password,$summ,$ymId).PHP_EOL.PHP_EOL;
} catch (\InvalidArgumentException $e){
    echo $e->getMessage().PHP_EOL;
}