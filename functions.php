<?php
/**
 * @param string $message сообщение которое нужно распарсить
 * array[0] string пароль
 * array[1] float сумма перевода
 * array[2] string кошелек
 * @return array результат в виде массива для дальшейней обработки
 */
function parseMessage(string $message): array
{
    $message = str_replace(["\n","\r"]," ",$message);

    //ищем пароль из 4 цифр
    preg_match('/([0-9]{6}[\s])/u', $message,$passwordMatch);
    if(empty($passwordMatch)){
        //если не нашли - ищем пароль из 6 цифр, самое предсказуемое изменение
        preg_match('/([0-9]{4}[\s])/u', $message,$passwordMatch);
    }
    if(empty($passwordMatch)){
        throw new \InvalidArgumentException("Неверный формат сообщения - не удалось найти пароль");
    }
    $password = $passwordMatch[0];

    //вынуть сумму с разделителем запятая, точка и если его нет
    preg_match('/([0-9]{1,6}([\,|\.]){0,1}[0-9]{0,2}р)/u', $message,$summMathc);
    if(empty($summMathc)){
        throw new \InvalidArgumentException("Неверный формат сообщения - не удалось найти сумму списания");
    }
    //на случай разных разделителей заменим его
    $summ = floatval(str_replace(",",".",$summMathc[0]));

    preg_match('/([0-9]{14,15})/u', $message,$ymIdMatch);
    if(empty($ymIdMatch)){
        throw new \InvalidArgumentException("Неверный формат сообщения - не удалось найти кошелек адресата");
    }
    $ymId = $ymIdMatch[0];
    return [$password,$summ,$ymId];
}